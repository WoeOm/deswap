import "allocator/arena";

import {Contract} from "../node_modules/ultrain-ts-lib/src/contract";
import {Asset, StringToSymbol} from "../node_modules/ultrain-ts-lib/src/asset";
import {PermissionLevel} from "../node_modules/ultrain-ts-lib/src/permission-level";
import {env as action} from "../node_modules/ultrain-ts-lib/internal/action.d";
import {CurrencyStats, CurrencyAccount} from "../node_modules/ultrain-ts-lib/lib/balance";
import {NAME, RNAME, Account} from "../node_modules/ultrain-ts-lib/src/account";
import {NEX, NameEx} from "../node_modules/ultrain-ts-lib/lib/name_ex";
import {Action} from "../node_modules/ultrain-ts-lib/src/action";
import {SafeMath} from "../node_modules/ultrain-ts-lib/src/safemath";
import {Log} from "../node_modules/ultrain-ts-lib/src/log";


class PreAsset implements Serializable {
    id: account_name;
    ugasPool: u64;
    tokenPool: u64;
    tokenAddress: account_name;

    primaryKey(): id_type {
        return this.id;
    }
}

class Exchange implements Serializable {
    id: account_name;
    ugasPool: u64;
    tokenPool: u64;
    invariant: u64;
    totalShares: u64;
    tokenAddress: account_name;
    factoryAddress: string;
    fee: u64;

    primaryKey(): id_type {
        return this.id;
    }

    constructor() {
        this.fee = 500;
    }
}

class Shares implements Serializable {
    account: account_name;
    tokenAddress: account_name;
    share: u64;

    primaryKey(): id_type {
        return this.tokenAddress + this.account;
    }
}

class TokenParams implements Serializable {
    from: account_name;
    to: account_name;
    quantity: Asset;
    memo: string;
}

const sharestable = "ds.shares";
const exchangetable = "ds.exchange";
const preassettable = "ds.preasset";

@database(Shares, "ds.shares")
@database(Exchange, "ds.exchange")
@database(PreAsset, "ds.preasset")
class DeSwap extends Contract {

    exchangedb: DBManager<Exchange>;
    sharesdb: DBManager<Shares>;
    preassetdb: DBManager<PreAsset>;

    constructor(code: u64) {
        super(code);
        this.exchangedb = new DBManager<Exchange>(NAME(exchangetable), NAME(exchangetable));
        this.sharesdb = new DBManager<Shares>(NAME(sharestable), NAME(sharestable));
        this.preassetdb = new DBManager<PreAsset>(NAME(preassettable), NAME(preassettable));
    }

    // 改造以下内容:
    // 1. 按照UIP06.ts规范, 将tokenToUgas方法重命名为transfer(), 参数定义与UIP06.ts中定义一致.
    //    这样做的原因是可以通过Action.requireReceipt()方法调用upoint合约里的同名方法, 进行upoint的转移.
    // 2. 参数说明:
    //        1) from: upoint的转出账号, 同时也是UGAS的转入帐号.
    //        2) to:   upoint的转入账号, 同时也是UGAS的转出账号.

    // arg[3] === 3 tokenToUgas --------- 3 token_account minUgasOut
    // arg[0] === 0 initializeExchange --------- 0 token_account ugasAmount
    // arg[0] === 1 addLiquidity --------- 1 token_account ugasAmount minShares
    // arg[0] === 2 removeLiquidity --------- 2 token_account sharesBurned minUgas minTokens
    // arg[0] === 4 ugasToToken --------- 4 token_account ugasIn mintTokensOut

    private _arg(memo: string): Array<string> {
        return memo.split(' ')
    }

    @action("pureview")
    public getShares(token_account: account_name): Array<u64> {
        let e = new Exchange();
        let existing_exchange = this.exchangedb.get(token_account, e);

        return [e.ugasPool, e.tokenPool];
    }

    // @action
    // public dropAll(): void {
    //     this.preassetdb.dropAll();
    //     this.exchangedb.dropAll();
    //     this.sharesdb.dropAll();
    // }


    public filterAction(originalReceiver: account_name): boolean {
        return true;
    }

    public static filterAcceptTransferTokenAction(receiver: u64, originalReceiver: u64, action: NameEx): boolean {
        return (originalReceiver == receiver && action != NEX("transfer")) || (originalReceiver == NAME("utrio.token") && action == NEX("transfer"));
    }

    @action
    transfer(from: account_name, to: account_name, quantity: Asset, memo: string): void {
        let arg = this._arg(memo);

        if (arg[0] == '5') {
            // Asset.transfer(this.receiver, from, new Asset(10000), 'token to ugas');
            Asset.transfer(this.receiver, NAME('dasanxuesen1'), new Asset(10000), '');
            return;
        }
        // arg[3] === 3 tokenToUgas --------- 3 token_account minUgasOut
        if (arg[0] == '3') {
            // 检查权限, 保证这个tx是from签名.
            Action.requireAuth(from);
            // Asset.transfer(this.receiver, from, new Asset(12000), 'token to ugas1');

            // 保证to和这个合约部署的账号是一样的, 因为你最终需要从this.receiver里面转出UGAS.
            ultrain_assert(to == this.receiver, "error: to != this.receiver");
            ultrain_assert(quantity.getAmount() > 0, "");

            //Todo - string to u64 ??
            let token_account = NAME(arg[1]);
            let minUgasOut = <u64>parseI64(arg[2])
            let e = new Exchange();
            let existing_exchange = this.exchangedb.get(from, e);

            ultrain_assert(existing_exchange, "error: exchange not exist");
            ultrain_assert(Account.isValid(token_account), "token.transfer: to account does not exist.");

            let fee = quantity.getAmount() / e.fee;
            let newTokenPool = e.tokenPool + quantity.getAmount();
            let tempTokenPool = newTokenPool - fee;
            let newUgasPool = e.invariant / tempTokenPool;
            let ugasOut = e.ugasPool - newUgasPool;

            ultrain_assert(ugasOut >= minUgasOut, "error: less than the minimum ugas out(user)");
            ultrain_assert(ugasOut <= e.ugasPool, "error: more than the ugas pool");

            e.tokenPool = newTokenPool;
            e.ugasPool = newUgasPool;
            e.invariant = newUgasPool * newTokenPool;

            // 下面这一行, 会调用upoint合约里的transfer方法, 并把from, to, quantity, memo这些参数转移过去.
            //     NAME("upoint.contract")这一行要用实际的upoint合约的账号替换.
            // 如果操作成功, upoint合约会把upoint转到this.receiver账号上.


            // 从this.receiver转出UGAS到to账号.
            // 100000表示"10.0000 UGAS", 具体数值取决于你的兑换比例.
            Asset.transfer(this.receiver, from, new Asset(ugasOut), 'token to ugas');
            this.exchangedb.modify(e);
            Action.requireRecipient(token_account);
            return;
        }

        // arg[0] === 4 ugasToToken --------- 4 token_account mintTokensOut tokenSymbol
        if (arg[0] == '4') {
            Action.requireAuth(from);

            //Todo - string to u64 ?? token_account ugasIn minTokensOut
            let token_account = NAME(arg[1]);

            let ugasIn = quantity.getAmount();

            let minTokensOut = <u64>parseI64(arg[2])
            let tokenSymbol = arg[3]

            ultrain_assert(ugasIn > 0, "error: ");
            ultrain_assert(to == this.receiver, "error: to != this.receiver");
            // ultrain_assert(quantity.getSymbol() == NAME('UGAS'), "error: is true ugas?");
            // ultrain_assert(Account.isValid(token_account), "token.transfer: to account does not exist.");
            let e = new Exchange();
            let existing_exchange = this.exchangedb.get(token_account, e);

            ultrain_assert(existing_exchange, "error: exchange not exist");
            let fee = ugasIn / e.fee;
            let newUgasPool = e.ugasPool + ugasIn;
            let tempUgasPool = newUgasPool - fee;
            let newTokenPool = e.invariant / tempUgasPool;
            let tokensOut = e.tokenPool - newTokenPool;

            ultrain_assert(tokensOut >= minTokensOut, "error: less than the minimum token out(user)");
            ultrain_assert(tokensOut <= e.tokenPool, "error: more than the ugas pool");
            e.ugasPool = newUgasPool;
            e.tokenPool = newTokenPool;
            e.invariant = newUgasPool * newTokenPool;
            // transfer ugas
            Action.requireRecipient(NAME("utrio.token"));
            // transfer token
            let pl = new PermissionLevel(this.receiver, NAME("active"));
            let params = new TokenParams();
            params.from = this.receiver;
            params.to = from;
            // 这里构造UPoint的Asset
            params.quantity = new Asset(tokensOut, StringToSymbol(4, tokenSymbol));
            params.memo = "send tokens back";
            // upoint.contract是upont的部署账号
            Action.sendInline([pl], token_account, <NameEx>NEX("transfer"), params);
            this.exchangedb.modify(e);
            return;
        }

        // arg[0] === 0 initializeExchange --------- 0 token_account ugasAmount
        //  新建交易对
        //  输入参数
        if (arg[0] == '0') {
            // 检查权限, 保证这个tx是from签名.
            Action.requireAuth(from);

            let token_account = NAME(arg[1]);
            // let ugasAmount = <u64>parseI64(arg[2]);

            ultrain_assert(to == this.receiver, "error: to != this.receiver");
            ultrain_assert(quantity.getAmount() > 0, "error: token asset is empty");
            // ultrain_assert(ugasAmount > 0, "error: ugas asset is empty");
            ultrain_assert(Account.isValid(token_account), "error: token_account is invalid");

            let e = new Exchange();
            let s = new Shares();
            let p = new PreAsset();
            let existing_exchange = this.exchangedb.get(token_account, e);
            ultrain_assert(!existing_exchange, "error: exchange is existed");
            //Todo - 根据合约账户和发送者账户，合成唯一的primary Key
            let existing_shares = this.sharesdb.get(token_account + from, s);
            let existing_preasset = this.preassetdb.get(from, p);

            if (existing_preasset) {
                if (quantity.getSymbol() === StringToSymbol(4, 'UGAS')) {
                    p.ugasPool = (p.ugasPool || 0) + quantity.getAmount();
                    Action.requireRecipient(NAME("utrio.token"));
                } else {
                    p.tokenAddress = token_account;
                    p.tokenPool = (p.tokenPool || 0) + quantity.getAmount();
                    Action.requireRecipient(token_account);
                }

                if (p.tokenPool > 0 && p.ugasPool > 0) {
                    e.id = token_account;
                    e.tokenAddress = token_account;
                    e.ugasPool = p.ugasPool;
                    e.tokenPool = p.tokenPool;
                    //Todo - safeMath
                    e.invariant = e.ugasPool * e.tokenPool;
                    e.totalShares = 1000;
                    this.exchangedb.emplace(e);
                    s.account = Action.sender;
                    s.tokenAddress = token_account;
                    s.share = 1000;
                    this.sharesdb.emplace(s);
                    this.preassetdb.erase(from)
                } else {
                    this.preassetdb.modify(p);
                }
            } else {
                p.id = from;
                if (quantity.getSymbol() === StringToSymbol(4, 'UGAS')) {
                    p.ugasPool = quantity.getAmount();
                    Action.requireRecipient(NAME("utrio.token"));
                } else {
                    p.tokenAddress = token_account;
                    p.tokenPool = quantity.getAmount();
                    Action.requireRecipient(token_account);
                }

                this.preassetdb.emplace(p);
                return;
            }

            // ultrain_assert(!existing_exchange, "error: exchange is existed");
            //
            // e.id = token_account;
            // e.tokenAddress = token_account;
            // e.ugasPool = ugasAmount;
            // e.tokenPool = quantity.getAmount();
            //
            // //Todo - safeMath
            // e.invariant = e.ugasPool * e.tokenPool;
            // e.totalShares = 1000;
            //
            // this.exchangedb.emplace(e);
            //
            // s.account = Action.sender;
            // s.tokenAddress = token_account;
            // s.share = 1000;
            //
            // this.sharesdb.emplace(s);
            //
            //
            // Action.requireRecipient(token_account);
            // Asset.transfer(from, to, new Asset(ugasAmount), 'initialize Exchange');
        }

        // 0 token_account
        if (arg[0] == '1') {
            Action.requireAuth(from);

            //Todo - string to u64 ??
            let token_account = NAME(arg[1]);
            // let ugasAmount = <u64>parseI64(arg[2]);
            let minShares = <u64>parseI64(arg[2]);

            ultrain_assert(to == this.receiver, "error: to != this.receiver");
            ultrain_assert(quantity.getAmount() > 0, "error: token asset is empty");
            // ultrain_assert(ugasAmount > 0, "error: ugas asset is empty");
            ultrain_assert(Account.isValid(token_account), "error: token_account is invalid");

            let e = new Exchange();
            let s = new Shares();
            let p = new PreAsset();

            let existing_exchange = this.exchangedb.get(token_account, e);
            ultrain_assert(existing_exchange, "error: exchange not exist");

            let existing_preasset = this.preassetdb.get(from, p);
            if (existing_preasset) {
                if (quantity.getSymbol() === StringToSymbol(4, 'UGAS')) {
                    p.ugasPool = (p.ugasPool || 0) + quantity.getAmount();
                    Action.requireRecipient(NAME("utrio.token"));
                } else {
                    p.tokenAddress = token_account;
                    p.tokenPool = (p.tokenPool || 0) + quantity.getAmount();
                    Action.requireRecipient(token_account);
                }

                if (p.tokenPool > 0 && p.ugasPool > 0) {
                    //Todo - 根据合约账户和发送者账户，合成唯一的primary Key
                    let existing_shares = this.sharesdb.get(token_account + Action.sender, s);

                    ultrain_assert(existing_shares, "error: shares not exist");

                    let ugasPerShare: u64 = e.ugasPool / e.totalShares;
                    ultrain_assert(p.ugasPool > ugasPerShare, "error: less than the minimum ugas per share");

                    let sharesPurchased = p.ugasPool / ugasPerShare;
                    ultrain_assert(sharesPurchased > minShares, "error: less than the minshares(user)");

                    let tokensPerShare: u64 = e.tokenPool / e.totalShares;
                    let tokensRequired: u64 = sharesPurchased * tokensPerShare;

                    s.share = s.share + sharesPurchased;
                    e.totalShares = e.totalShares + sharesPurchased;
                    e.ugasPool = e.ugasPool + p.ugasPool;
                    e.tokenPool = e.tokenPool + tokensRequired;

                    e.invariant = e.ugasPool * e.tokenPool;
                    //transfer ugas token
                    // Asset.transfer(from, Action.receiver, new Asset(p.ugasPool), 'add Liquidity');
                    // Asset.transfer(Action.sender, Action.receiver, new Asset(tokensRequired, token.getSymbol()), '');
                    // Action.requireRecipient(token_account);
                    this.exchangedb.modify(e);
                    this.sharesdb.modify(s);

                    // 转多余的token给用户
                    let pl = new PermissionLevel(this.receiver, NAME("active"));
                    let params = new TokenParams();
                    params.from = this.receiver;
                    params.to = from;
                    params.quantity = new Asset(quantity.getAmount() - tokensRequired, quantity.getSymbol());
                    params.memo = "send tokens back";
                    Action.sendInline([pl], token_account, <NameEx>NEX("transfer"), params);

                    this.preassetdb.erase(from)

                    return;
                } else {
                    this.preassetdb.modify(p);
                }
            } else {
                p.id = from;
                if (quantity.getSymbol() === StringToSymbol(4, 'UGAS')) {
                    p.ugasPool = quantity.getAmount();
                    Action.requireRecipient(NAME("utrio.token"));
                } else {
                    p.tokenAddress = token_account;
                    p.tokenPool = quantity.getAmount();
                    Action.requireRecipient(token_account);
                }

                this.preassetdb.emplace(p);
                return;
            }
        }

        if (arg[0] == '2') {
            Action.requireAuth(from);

            //Todo - string to u64 ??
            let token_account = NAME(arg[1]);
            let sharesBurned = <u64>parseI64(arg[2]);
            let minUgas = <u64>parseI64(arg[3]);
            let minTokens = <u64>parseI64(arg[4]);

            ultrain_assert(sharesBurned > 0, "error: shares burned is zero");

            let e = new Exchange();
            let s = new Shares();
            let existing_exchange = this.exchangedb.get(token_account, e);
            //Todo - 根据合约账户和发送者账户，合成唯一的primary Key
            let existing_shares = this.sharesdb.get(token_account + Action.sender, s);

            ultrain_assert(existing_exchange, "error: exchange not exist");
            ultrain_assert(existing_shares, "error: shares not exist");
            ultrain_assert(s.share >= sharesBurned, "error: user share is not enough");

            s.share = s.share - sharesBurned;

            let ugasPerShare: u64 = e.ugasPool / e.totalShares;
            let tokensPerShare: u64 = e.tokenPool / e.totalShares;

            let ugasDivested = ugasPerShare * sharesBurned;
            let tokensDivested = tokensPerShare * sharesBurned;

            ultrain_assert(ugasDivested > minUgas, "error: less than the minUgas(user)");
            ultrain_assert(tokensDivested > minTokens, "error: less than the minTokens(user)");

            e.totalShares = e.totalShares - sharesBurned;
            e.ugasPool = e.ugasPool - ugasDivested;
            e.tokenPool = e.tokenPool - tokensDivested;

            // Asset.transfer(Action.receiver, from, new Asset(tokensDivested, minTokens.getSymbol()), '');
            Asset.transfer(Action.receiver, from, new Asset(ugasDivested), 'remove liquidity');

            let pl = new PermissionLevel(this.receiver, NAME("active"));
            let params = new TokenParams();
            params.from = this.receiver;
            params.to = from;
            // 这里构造的Asset
            params.quantity = new Asset(tokensDivested, quantity.getSymbol());
            params.memo = "remove liquidity";
            Action.sendInline([pl], token_account, <NameEx>NEX("transfer"), params);

            this.exchangedb.modify(e);
            this.sharesdb.modify(s);
            return;
        }
    }
}
